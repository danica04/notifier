<?php

include("connect.php"); 	
	$link=Connection();
	
	
	
	 $myID = ($_COOKIE['patient_id']);
     
$sql="SELECT tbl_patient.patient_id, tbl_patient.Fname, tbl_patient.Mname, tbl_patient.Lname, tbl_patient.patient_address, tbl_patient.patient_number,tbl_relative.username,tbl_relative.password, tbl_patient.disease_id, tbl_clinic.clinic_name,tbl_clinic.Cm_name,tbl_clinic.Cl_name,tbl_clinic.clinic_address,tbl_clinic.clinic_number, tbl_clinic.clinic_email,tbl_relative.rel_name,tbl_relative.M_name,tbl_relative.L_name,tbl_relative.rel_name2,tbl_relative.M_name2,tbl_relative.L_name2,tbl_relative.rel_address,tbl_relative.rel_address2, tbl_relative.rel_number,tbl_relative.rel_number2 FROM tbl_patient, tbl_clinic, tbl_relative,tbl_disease WHERE tbl_patient.disease_id = tbl_disease.disease_id AND tbl_patient.clinic_id = tbl_clinic.clinic_id AND tbl_patient.rel_id = tbl_relative.rel_id AND tbl_patient.patient_id LIKE '%" . $myID . "%'";
    $result= mysqli_query($link,$sql);
    if($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    
	$fname= $row['Fname'];
	$mname= $row['Mname'];
	$lname= $row['Lname'];
    $disease= $row['disease_id'];
	$Paddress= $row['patient_address'];
	$Pnumber= $row['patient_number'];
        $Cname= $row['clinic_name'];
        $Cmname= $row['Cm_name'];
        $Clname= $row['Cl_name'];
        $Caddress= $row['clinic_address'];
        $Cnumber= $row['clinic_number'];
        $Cemail= $row['clinic_email'];
        $Rname= $row['rel_name'];
        $Rmname= $row['M_name'];
        $Rlname= $row['L_name'];
        
        $Rname2= $row['rel_name2'];
        $Rmname2= $row['M_name2'];
        $Rlname2= $row['L_name2'];
        $Raddress= $row['rel_address'];
        $Raddress2= $row['rel_address2'];
        $Rnumber= $row['rel_number'];
	    $Rnumber2= $row['rel_number2'];
        $id= $row['patient_id'];
	    $user= $row['username'];
	    $pass= $row['password'];
	    
    }

    $select2 = "SELECT disease FROM tbl_disease WHERE disease_id LIKE '%" . $disease . "%'";
    
    $result2= mysqli_query($link,$select2);
    if ($row = mysqli_fetch_array($result2,MYSQLI_ASSOC)) {
    $dname  = $row["disease"];
    
    
    $sql2="SELECT * FROM tbl_disease ORDER BY tbl_disease.disease_id";
	$result2= mysqli_query($link,$sql2);
    


    }
 ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.-->


<html>
    <head>
  
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Patient's Information</title>
      
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-responsive.css" />

    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/carousel.css" rel="stylesheet">
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        

        
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">E - Notifier Wristband: Chronic Patients Tracker System</a>
        </div>
        <div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
            <li><a>ID:<?php echo "$myID"?></a></li>
            </ul>
          
          
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="index.php">Home</a></li>
            <li class="active"><a href="client.php">My Profile</a></li>
            <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Edit Profile<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#CP" data-toggle="modal"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span> Change Password</a></li>
                                <li><a href="#PIedit" data-toggle="modal"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span> Patient's Information</a></li>
                                <li><a href="#DIedit" data-toggle="modal"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span> Doctor's Information</a></li>
                                <li><a href="#RIedit" data-toggle="modal"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span> Relative's Information</a></li>
                                <li><a href="#RIedit2" data-toggle="modal"><span class='glyphicon glyphicon-edit' aria-hidden='true'></span> 2nd Relative's Information</a></li>
                                
                            </ul>
                        </li>
            <li><a href="map.php">History</a></li>
            <li><a href="logout.php">Logout</a></li>        
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<br>
          
			<div class="container">
          <div class="row">
                   
                  <h3><br>Patient's Information</h3>
                  <div class="form-group">
                   
                                <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="fname">Fullname:</label>
                                    <input type="text" class="form-control" id="fullname" name="fullname" readonly value="<?php echo "$fname $mname $lname"?>" required autofocus>
                                </div>
                                
				                <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="fname">Contact:</label>
                                    <input type="text" class="form-control" id="Pcontact" name="Pcontact" readonly value="<?php echo "$Pnumber"?>" required autofocus>
                                </div>
                               <br><br><br><br><br>
                                <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="fname">Disease:</label>
                                    <input type="text" class="form-control" id="Pdisease" name="Pdisease" readonly value="<?php echo "$dname"?>" required autofocus>
                                </div>
                                <div class="col-sm-3">
                                     <label class="control-label col-sm-2" for="address">Address:</label>
                                    <textarea readonly class="form-control" id="address" name="address" required style="width: 100%;"><?php echo "$Paddress"?></textarea>
                                </div>
                                
                                
                                
                               
                  </div>
                     <br><br><br><br>
				  <h3>Doctor's Detailed</h3><br>
                        <div class="form-group">
                               <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="dname">Name:</label>
                                    <input type="text" class="form-control" id="dname" name="fdname" readonly value="<?php echo "$Cname $Cmname $Clname"?>"placeholder="First Name" required autofocus>
                                </div>
                                
				<div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="fname">Contact:</label>
                                    <input type="text" class="form-control" id="fullname" name="fname" readonly value="<?php echo "$Cnumber"?>"placeholder="First Name" required autofocus>
                                </div>
                                 <br><br> <br><br>
                               <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="fname">Email:</label>
                                    <input type="text" class="form-control" id="fullname" name="fname" readonly value="<?php echo "$Cemail"?>"placeholder="First Name" required autofocus>
                                </div>
                                <div class="col-sm-3">
                                     <label class="control-label col-sm-4" for="address">Address:</label>
                                    <textarea readonly class="form-control" id="address" name="address" placeholder="Address" required style="width: 100%;"><?php echo "$Caddress"?></textarea>
                                </div>				
                  </div>
                     <br><br><br>
            <h3>Relative's Detailed</h3><br>
            <h4>Relative 1:</h4>
                  <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="dname">Name:</label>
                                    <input type="text" class="form-control" id="dname" name="fdname" readonly value="<?php echo "$Rname $Rmname $Rlname"?>"placeholder="First Name" required autofocus>
                                </div>
                                
                                <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="fname">Contact:</label>
                                    <input type="text" class="form-control" id="fullname" name="fname" readonly value="<?php echo "$Rnumber"?>"placeholder="First Name" required autofocus>
                                </div>
                                 <br><br> <br><br>
                                <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="User">Username:</label>
                                    <input type="text" class="form-control" id="User" name="username" readonly value="<?php echo "$user"?>"placeholder="First Name" required autofocus>
                                </div>
                                 <!--<div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="User">Password:</label>
                                    <input type="text" class="form-control" id="User" name="username" readonly value="<?php echo "$pass"?>" required autofocus>
                                </div><br><br><br><br>  --> 
                                <div class="col-sm-3">
                                     <label class="control-label col-sm-4" for="address">Address:</label>
                                    <textarea readonly class="form-control" id="address" name="address" placeholder="Address" required style="width: 100%;"><?php echo "$Raddress"?></textarea>
                                </div><br><br><br><br>

                                <h4>Relative 2:</h4>
                  <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="dname">Name:</label>
                                    <input type="text" class="form-control" id="dname" name="fdname" readonly value="<?php echo "$Rname2 $Rmname2 $Rlname2"?>"placeholder="First Name" required autofocus>
                                </div>
                                
                                <div class="col-sm-3">
                                    <label class="control-label col-sm-2" for="fname">Contact:</label>
                                    <input type="text" class="form-control" id="fullname" name="fname" readonly value="<?php echo "$Rnumber2"?>"placeholder="First Name" required autofocus>
                                </div>
                                 <br><br> <br><br>
                                   
                                <div class="col-sm-3">
                                     <label class="control-label col-sm-4" for="address">Address:</label>
                                    <textarea readonly class="form-control" id="address" name="address" placeholder="Address" required style="width: 100%;"><?php echo "$Raddress2"?></textarea>
                                </div>

            

                                   
                                </div>



            </div>
            </div>
        </div>    
				  
                  
                  
             
          </div>
              
         
          
        </div>
          
          
        </div>
      </div>
    </div>
    
    
      <!--Edit CP Modal -->
        <div id="CP" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Change Password</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">Current:</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="current_password" required placeholder="Current Password" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">New:</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="new_password" required placeholder="New Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">Repeat:</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="repeat_password" required placeholder="Repeat Password">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" name="change_pass">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    
     <!--Edit PI Modal -->
        <div id="PIedit" class="modal fade" role="dialog">
            <form method="post" class="form-horizontal" role="form">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Personal Information</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="id" value="">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="firstname">Firstname:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo "$fname"?>" placeholder="First Name" required autofocus>
                                </div>
                                <label class="control-label col-sm-2" for="lastname">Lastname:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo "$lname"?>" placeholder="Last Name" required>
                                </div>
								
                            </div>
                            <div class="form-group">
				<label class="control-label col-sm-2" for="midname">Middle Initial:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="midname" name="midname" value="<?php echo "$mname"?>" placeholder="Middle Name" required>
                                </div>
                                <label class="control-label col-sm-2" for="Paddress">Address:</label>
                                <div class="col-sm-4">
                                    <textarea cclass="form-control" id="Paddress" name="Paddress" placeholder="Address" required style="width: 100%;"><?php echo "$Paddress"?></textarea>
                                </div>
                                <label class="control-label col-sm-2" for="Pcontact">Contact #:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="Pcontact" name="Pcontact" value="<?php echo "$Pnumber"?>" placeholder="Contact number" required>
                                </div>
                                <label class="control-label col-sm-2" for="disease">Disease:</label>
                                <div class="col-sm-4">
                                    <select class="form-control" name="cmbDisease" onchange="document.getElementById('selected_text').value=this.options[this.selectedIndex].text">
   <?php
   
    while($row=mysqli_fetch_array($result2)){                                                 
       echo "<option value='".$row['disease_id']."'>".$row['disease']."</option>";
    }
?></select><br>
    <input type="hidden" name="selected_text" id="selected_text" value="" />

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" name="update_info"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
		
		<!--Edit DI Modal -->
        <div id="DIedit" class="modal fade" role="dialog">
            <form method="post" class="form-horizontal" role="form">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Doctor's Detailed</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="id" value="">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="firstname">Firstname:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo "$Cname"?>" placeholder="First Name" required autofocus>
                                </div>
                                <label class="control-label col-sm-2" for="Mname">Middle Initial:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="Mname" name="Mname" value="<?php echo "$Cmname"?>" placeholder="Middle Initial" required autofocus>
                                </div><br><br><br>
                                <label class="control-label col-sm-2" for="Lname">Lastname:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="Lname" name="Lname" value="<?php echo "$Clname"?>" placeholder="Lastname" required autofocus>
                                </div>
                                <label class="control-label col-sm-2" for="contact">Contact #:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="contact" name="contact" value="<?php echo "$Cnumber"?>" placeholder="Contact number" required>
                                </div><br><br>
                                
								
								
                            </div>
                            <div class="form-group">
                                
								<label class="control-label col-sm-2" for="email">Email:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo "$Cemail"?>" placeholder="Email" required>
                                </div>
                                <label class="control-label col-sm-2" for="address">Address:</label>
                                <div class="col-sm-4">
                                    <textarea cclass="form-control" id="address" name="address" placeholder="Address" required style="width: 100%;"><?php echo "$Caddress"?></textarea>
                                </div>                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" name="update_DI"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
		
		
		<!--Edit RI Modal -->
        <div id="RIedit" class="modal fade" role="dialog">
            <form method="post" class="form-horizontal" role="form">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Relative's Detailed</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="id" value="">
                            <div class="form-group">
                                
                                <label class="control-label col-sm-2" for="fname">First Name:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="Fname" name="Fname" value="<?php echo "$Rname"?>" placeholder="First Name" required autofocus>
                                </div>
                                <label class="control-label col-sm-2" for="Mname">Middle Initial:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="Mname" name="Mname" value="<?php echo "$Rmname"?>" placeholder="Middle Initial" required autofocus></div><br><br><br>
                                <label class="control-label col-sm-2" for="Lname">Last Name:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="Lname" name="Lname" value="<?php echo "$Rlname"?>" placeholder="Lastname" required autofocus>
                                </div>
                                <label class="control-label col-sm-2" for="contact">Contact #:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="contact" name="contact" value="<?php echo "$Rnumber"?>" placeholder="Contact number" required>
                                </div><br><br>
								
                            </div>
                            <div class="form-group">
								<label class="control-label col-sm-2" for="user">Username:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="username" name="username" value="<?php echo "$user"?>" placeholder="Username" required>
                                </div>
                                <label class="control-label col-sm-2" for="address">Address:</label>
                                <div class="col-sm-4">
                                    <textarea class="form-control" id="address" name="address" placeholder="Address" required style="width: 100%;"><?php echo "$Raddress"?></textarea>
                                </div>                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" name="update_RI"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
        
        
        <!--Edit RI2 Modal -->
        <div id="RIedit2" class="modal fade" role="dialog">
            <form method="post" class="form-horizontal" role="form">
                <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Relative Detailed</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="id" value="">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="firstname">Firstname:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="firstname" name="Fname" value="<?php echo "$Rname2"?>" placeholder="First Name" required autofocus>
                                </div>
                                <label class="control-label col-sm-2" for="Mname">Middle Initial:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="Mname" name="Mname" value="<?php echo "$Rmname2"?>" placeholder="Middle Initial" required autofocus>
                                </div><br><br><br>
                                <label class="control-label col-sm-2" for="Lname">Lastname:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="Lname" name="Lname" value="<?php echo "$Rlname2"?>" placeholder="Lastname" required autofocus>
                                </div>
                                <label class="control-label col-sm-2" for="contact">Contact #:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="contact" name="contact" value="<?php echo "$Rnumber2"?>" placeholder="Contact number" required>
                                </div><br><br><br>
                                <label class="control-label col-sm-2" for="address">Address:</label>
                                <div class="col-sm-4">
                                    <textarea cclass="form-control" id="address" name="address" placeholder="Address" required style="width: 100%;"><?php echo "$Raddress2"?></textarea>
                                </div>    
                                
								
								
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" name="update_RI2"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle"></span> Cancel</button>
                        </div>
                    </div>
                </div>
        </div>
        </form>
        </div>
		
    <?php 
    //changepass
      $myID = ($_COOKIE['patient_id']);
    
	if(isset($_POST['change_pass'])){
	     $current_password = $_POST['current_password'];
    $new_password = $_POST['new_password'];
    $repeat_password = $_POST['repeat_password'];
                            $sql = "SELECT password FROM tbl_relative, tbl_patient WHERE tbl_patient.rel_id = tbl_relative.rel_id AND tbl_patient.patient_id LIKE '%" . $myID . "%'";

                           

                                
        if( $result2 = mysqli_query($link, $sql)){
         if(mysqli_num_rows($result2)>0)
            {
                while ($row = mysqli_fetch_array($result2)) {
                    if($row['password'] != $current_password)
                    {                        
                        echo "<script>window.alert('Invalid Password');</script>";
                                        $passwordErr = '<div class="alert alert-warning">
                        <strong>Password!</strong> Invalid.
                        </div>';
                    }
                    elseif($new_password != $repeat_password) {
                                        echo "<script>window.alert('Password Not Match!');</script>";
                                        $passwordErr = '<div class="alert alert-warning">
                        <strong>Password!</strong> Not Match.
                        </div>';
                                    } else{
                                        $sql2 = "UPDATE tbl_relative,tbl_patient SET password='$new_password' WHERE tbl_patient.rel_id = tbl_relative.rel_id AND tbl_patient.patient_id LIKE '%" . $myID . "%'";

                                        if ($result = mysqli_query($link, $sql2)) {
                                            echo "<script>window.alert('Password Successfully Updated');</script>";
                                        } else {
                                            echo "Error updating information";
                                        }
                                    }    
                                }
                            } else {
                                $usernameErr = '<div class="alert alert-danger">
          <strong>Username</strong> Not Found.
          </div>';
                                $username = "";
                            }
                        }

	}               
                
                
                
                //Update PI
                        if(isset($_POST['update_info'])){
                            
                            $edit_Fname = $_POST['firstname'];
                            $edit_Mname = $_POST['midname'];
                            $edit_Lname = $_POST['lastname'];
                            $edit_Paddress = $_POST['Paddress'];
                            $edit_Pnumber= $_POST['Pcontact'];
                             $disease2= $_POST["selected_text"];
                             $select = "SELECT disease_id FROM tbl_disease where disease LIKE '%" . $disease2 . "%'";
	
	$result= mysqli_query($link,$select);
	if ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    $Did  = $row["disease_id"];
    
	}


                            $sql = "UPDATE tbl_patient SET 
                                Fname='$edit_Fname',
                                Mname='$edit_Mname',
                                Lname='$edit_Lname',
                                patient_address='$edit_Paddress',
                                patient_number='$edit_Pnumber',
                                disease_id='$Did'
                                WHERE patient_id LIKE '%" . $myID . "%'";
                                
                                
                               $result = mysqli_query($link,$sql); 	
                           if ($result!==FALSE) {
                               
                                echo "<script>window.alert('Information Successfully Updated');</script>";
                            } else {
                                echo "Error updating information";
                            }
                          
                        }
 
             
        //Update DI
                        if(isset($_POST['update_DI'])){
                            
                            $edit_Fname = $_POST['firstname'];
                            $edit_Mname = $_POST['Mname'];
                            $edit_Lname = $_POST['Lname'];
                            $edit_email = $_POST['email'];
                            $edit_Caddress = $_POST['address'];
                            $edit_Cnumber= $_POST['contact'];
                            $sql = "UPDATE tbl_clinic,tbl_patient SET 
                                clinic_name='$edit_Fname',
                                Cm_name='$edit_Mname',
                                Cl_name='$edit_Lname',
                                clinic_email='$edit_email',
                                clinic_address='$edit_Caddress',
                                clinic_number='$edit_Cnumber'
                               WHERE tbl_patient.clinic_id = tbl_clinic.clinic_id AND tbl_patient.patient_id LIKE '%" . $myID . "%'";
                                
                                
                               $result = mysqli_query($link,$sql); 	
                           if ($result!==FALSE) {
                                echo "<script>window.alert('Information Successfully Updated');</script>";
                            } else {
                                echo "Error updating information";
                            }
                          
                        }
                        
         //Update RI
                        if(isset($_POST['update_RI'])){
                            
                            $edit_Fname = $_POST['Fname'];
                            $edit_Mname = $_POST['Mname'];
                            $edit_Lname = $_POST['Lname'];
                            $edit_username = $_POST['username'];
                            $edit_Raddress = $_POST['address'];
                            $edit_Rnumber= $_POST['contact'];
                            $sql = "UPDATE tbl_relative,tbl_patient SET 
                                rel_name='$edit_Fname',
                                M_name='$edit_Mname',
                                L_name='$edit_Lname',
                                username='$edit_username',
                                rel_address='$edit_Raddress',
                                rel_number='$edit_Rnumber'
                               WHERE tbl_patient.rel_id = tbl_relative.rel_id AND tbl_patient.patient_id LIKE '%" . $myID . "%'";
                                
                                
                               $result = mysqli_query($link,$sql); 	
                           if ($result!==FALSE) {
                                echo "<script>window.alert('Information Successfully Updated');</script>";
                            } else {
                                echo "Error updating information";
                            }
                          
                        }
            //update R2
             //Update RI
                        if(isset($_POST['update_RI2'])){
                            
                            $edit_Fname = $_POST['Fname'];
                            $edit_Mname = $_POST['Mname'];
                            $edit_Lname = $_POST['Lname'];
                            $edit_Raddress = $_POST['address'];
                            $edit_Rnumber= $_POST['contact'];
                            $sql = "UPDATE tbl_relative,tbl_patient SET 
                                rel_name2='$edit_Fname',
                                M_name2='$edit_Mname',
                                L_name2='$edit_Lname',
                                rel_address2='$edit_Raddress',
                                rel_number2='$edit_Rnumber'
                               WHERE tbl_patient.rel_id = tbl_relative.rel_id AND tbl_patient.patient_id LIKE '%" . $myID . "%'";
                                
                                
                               $result = mysqli_query($link,$sql); 	
                           if ($result!==FALSE) {
                                echo "<script>window.alert('Information Successfully Updated');</script>";
                            } else {
                                echo "Error updating information";
                            }
                          
                        }
             
        
    
	
    
 ?>
	
     <?php 
     $myID = ($_COOKIE['patient_id']);
echo "$myID"; ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    </body>
</html>