<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
	include("connect.php"); 	
	$link=Connection();
?>  
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <style>
           #map {
            height: 400px;
            width: 100%;
             margin: 2%;
            
           }
        </style>
        
    
        <title>Welcome To Admin Page Demonstration</title>
</head>

      
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/bootstrap-responsive.css" />

        <link href="css/dashboard.css" rel="stylesheet">
        <link href="css/carousel.css" rel="stylesheet">
            
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <body onload="load()">
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">E - Notifier Wristband: Chronic Patients Tracker System</a>
        </div>
        <div class="navbar-collapse collapse">      
          <form class="navbar-form navbar-right">
            <input type="hidden" id="hiddenDATE" class="form-control" value="<?php echo date('Y-m-d');?>">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class="active"><a href="admin.php">Home</a></li>
                    <li><a href="adminLogout.php">Logout</a></li> 
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <div class="row">                   
                    <div class="col-sm-6 col-md-12">
                        <?php
                            $sql="SELECT * FROM tbl_patient LEFT JOIN tbl_disease ON tbl_patient.disease_id = tbl_disease.disease_id";
                            if($result = mysqli_query($link, $sql)){
                                if(mysqli_num_rows($result)>0)
                                {
                                    while ($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {

                        
                        ?>
                           <div class="thumbnail">
                                <div class="caption">
                                    <h3><?php echo $row['Fname']." ".$row['Mname']." ".$row['Lname']; ?></h3>
                                    <p>Address: <?php echo $row['patient_address']; ?></p>
                                    <p>Phone No.: <?php echo $row['patient_number']; ?></p>
                                    <p>Disease/s: <?php echo $row['disease']; ?></p>
                                    <p>Username: <?php echo $row['username']; ?></p>
                                    
                                    
                                    
                            </div>
                            
                            
              <body>
             <p><a href="adminLogout.php"></a></p>
        </body>
                        <?php
                                        }
                                    }
                                }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    </body>
</body>
</head>
</html>