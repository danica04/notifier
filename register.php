<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php

include("connect.php"); 	
	$link=Connection();
	

$sql="SELECT * FROM tbl_disease ORDER BY tbl_disease.disease_id";
	$result= mysqli_query($link,$sql);
    



?>
<html>
    <head>
        <title>Register</title>
        
         <link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/bootstrap-responsive.css" />
	<link rel="stylesheet" href="css/signin.css" />
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="login.php">E - Notifier Wristband: Chronic Patients Tracker System</a>
        </div>
       
      </div>
    </div>
        <br><br>
        
        <div class="container">
   
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <fieldset>
                    <legend>Patient Information</legend>
               
            <div class="form-group">
        <form action="reg1.php" method="POST" name="register">
	
   FirstName: <input class="form-control" type="text" name="fname" id="fname" size="20" maxlength="10" required autofocus><br />
    MiddleInitial: <input class="form-control" type="text" name="mname" id="mname" size="20" maxlength="2"><br />
    LastName: <input class="form-control" type="text" name="lname" id="lname" size="20" maxlength="15" required><br />
    Contact#: +63 <input class="form-control" type="text" name="number" id="number" size="20" maxlength="13" required><br />
    Address: <input class="form-control" type="text" name="address" id="address" size="20" maxlength="15" required><br />
   Disease:
   <select class="form-control" name="cmbDisease" onchange="document.getElementById('selected_text').value=this.options[this.selectedIndex].text">
   <?php
   
    while($row=mysqli_fetch_array($result)){                                                 
       echo "<option value='".$row['disease_id']."'>".$row['disease']."</option>";
    }
?></select><br>
   
    <input type="hidden" name="selected_text" id="selected_text" value="" />
    <a href="index.php">Back</a>
    <br /><br />
    <input type="submit" value="Next Page" class="btn btn-primary btn-lg">
        </form>
            </div>
                </fieldset>
            </div>
            <div class="col-lg-3"></div>
        </div>
        </div>
    </body>
</html>