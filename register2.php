<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Register</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/bootstrap-responsive.css" />
	<link rel="stylesheet" href="css/signin.css" />
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        
        
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="login.php">E - Notifier Wristband: Chronic Patients Tracker System</a>
        </div>
       
      </div>
    </div>
        <br><br>
        
        
        
        <div class="container">
   
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <fieldset>
                    <legend>Doctor's Information</legend>
               
            <div class="form-group">
        <form action="reg2.php" method="POST" name="register">
	
    FirstName: <input class="form-control" type="text" name="C_name" size="20" maxlength="10"><br />
    MiddleInitial: <input class="form-control" type="text" name="M_name" size="20" maxlength="2"><br />
    LastName: <input class="form-control" type="text" name="L_name" size="20" maxlength="15"><br />
    Address: <input class="form-control" type="text" name="C_address" size="20" maxlength="15"><br />
    Contact#: +63 <input class="form-control" type="text" name="C_number" size="20" maxlength="13"><br />
    Email: <input class="form-control" type="text" name="C_email" size="20" maxlength="20"><br />
   

   
   <!-- <a href="register3.php">Escape</a>  -->  
    <br /><br />
    <input type="submit" value="Next Page" class="btn btn-primary btn-lg">
        </form>
            </div>
                </fieldset>
            </div>
            <div class="col-lg-3"></div>
        </div>
        </div>
    </body>
</html>