<?php

include("connect.php"); 	
require("fpdf/fpdf.php");

	

$pdf = new FPDF();	

class PDF extends FPDF
{
	// Load data
// Page header
function Header()
{
 	$date = date("j-n-Y"); 
$this->SetFont("Arial","B",16);
$this->Cell(0,10,"Patient List Report",0,1,"C");
$this->SetFont("Arial","",14);
$this->Cell(0,10,"Date: $date",0,1,"C");
$this->Cell(0,10,"",0,1,"");
}

function Mybody()
{
		$link=Connection();
	
		$this->SetFont('Times','B',10);
		$this->Cell(8,7,'ID',1,0,'C');
		$this->Cell(35,7,'Name',1,0,'C');
		$this->Cell(35,7,'Address',1,0,'C');
		$this->Cell(25,7,'Contact',1,0,'C');
		$this->Cell(15,7,'Disease',1,0,'C');
		$this->Cell(25,7,'Doctor',1,0,'C');
		$this->Cell(25,7,'Relative',1,0,'C');
	$this->Cell(25,7,'Phone',1,0,'C');
		$this->Ln();
		
		$this->SetFont('Times','',9);
		$sql="SELECT tbl_patient.patient_id, tbl_patient.Fname, tbl_patient.Mname,tbl_patient.Lname,tbl_disease.disease, tbl_patient.patient_address, tbl_patient.patient_number, tbl_clinic.clinic_name, tbl_relative.rel_name, tbl_relative.rel_number
FROM tbl_patient, tbl_clinic, tbl_relative, tbl_disease
WHERE tbl_patient.clinic_id = tbl_clinic.clinic_id AND tbl_patient.disease_id = tbl_disease.disease_id
AND tbl_patient.rel_id = tbl_relative.rel_id AND tbl_patient.status = 'active'
ORDER BY tbl_patient.patient_id";
	$result= mysqli_query($link,$sql);
		 if($result!==FALSE){
		     while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
				$fname= $row['Fname'];
				$mname= $row['Mname'];
				$lname= $row['Lname'];
				$name= "$fname $mname $lname";
			$this->Cell(8,7,$row['patient_id'],1,0,'C');
			$this->Cell(35,7,$name,1,0,'C');
			$this->Cell(35,7,$row['patient_address'],1,0,'C');
			$this->Cell(25,7,$row['patient_number'],1,0,'C');
			$this->Cell(15,7,$row['disease'],1,0,'C');
			$this->Cell(25,7,$row['clinic_name'],1,0,'C');
			$this->Cell(25,7,$row['rel_name'],1,0,'C');
			$this->Cell(25,7,$row['rel_number'],1,0,'C');
			$this->Ln();
				}
		     mysqli_free_result($result);
		     mysqli_close($link);
		  }
	
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}


}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Mybody();
$pdf->Output();
?>