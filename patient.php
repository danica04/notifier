<?php

	include("connect.php"); 	
	
	$link=Connection();
	$sql="SELECT tbl_patient.patient_id, tbl_patient.Fname, tbl_patient.Lname, tbl_patient.patient_number, tbl_patient.patient_address, tbl_clinic.clinic_name, tbl_relative.rel_name, tbl_relative.rel_address, tbl_relative.rel_number, tbl_disease.disease
FROM tbl_patient, tbl_clinic, tbl_relative, tbl_disease
WHERE tbl_patient.clinic_id = tbl_clinic.clinic_id AND tbl_patient.disease_id = tbl_disease.disease_id
AND tbl_patient.rel_id = tbl_relative.rel_id AND tbl_patient.status = 'active'
ORDER BY tbl_patient.patient_id";
	$result= mysqli_query($link,$sql);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Patient's Information</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/bootstrap-responsive.css" />

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">

  
  </head>

  <body>
 

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">E - Notifier Wristband: Chronic Patients Tracker System</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            
          </ul>
          <form class="navbar-form navbar-right">
           
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           <!-- <li><a href="index.php">Home</a></li>-->
            <li class="active"><a href="patient.php">Patient's Information</a></li>
            <li><a href="form.php">Print Preview</a></li>
             <li><a href="logout.php">Logout</a></li>                   
 </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Patient's Information</h1>

          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Full Name</th>
                 <!-- <th>Last Name</th>-->
                 <th>Disease</th>
                  <th>Address</th>
                  <th>Contact</th>
                  <th>Doctor</th>
                  <th>Relative Name</th>
                  <th>Relative number</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
					 <?php 
		  if($result!==FALSE){
		     while($row = mysqli_fetch_array($result)) {
				?>
					<tr>
	
		<td><?php echo $row['Fname']; ?> <?php echo $row['Lname']; ?></td>
		<td><?php echo $row['disease']; ?></td>
		<td><?php echo $row['patient_address']; ?></td>
		<td><?php echo $row['patient_number']; ?></td>
		<td><?php echo $row['clinic_name']; ?></td>
		<td><?php echo $row['rel_name']; ?></td>
		<td><?php echo $row['rel_number']; ?></td>

		<td><a href="updateUser.php?id=<?php echo $row['patient_id']; ?>"<button type='button' class='btn btn-warning btn-sm'></button>Edit</a> | <a onclick="return confirm('Do you really to want Delete this Patient?')" href="delete.php?id=<?php echo $row['patient_id']; ?>" <button type='button' class='btn btn-danger btn-sm'></button>Delete</a></td>
	</tr>
		<?php			
				 }
		    // mysqli_free_result($result);
		    // mysqli_close($link);
		  }
                    ?>

			  
                
       
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
  </body>
</html>