<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php

	include("connect.php"); 	
	
	$link=Connection();
	$sql="SELECT tbl_data.date, tbl_data.latitude, tbl_data.longitude, tbl_patient.Fname, tbl_patient.Lname
FROM tbl_data, tbl_patient
WHERE tbl_data.patient_id = tbl_patient.patient_id
ORDER BY tbl_data.data_id;";
	$result= mysqli_query($link,$sql);
	if ($row = mysqli_fetch_array ($result,MYSQLI_ASSOC)) {
	
	    
	}
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Home</title>
      
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-responsive.css" />

    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/carousel.css" rel="stylesheet">
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        

        
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.php">E - Notifier Wristband: Chronic Patients Tracker System</a>
        </div>
        <div class="navbar-collapse collapse">
          
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="home.php">Home</a></li>
            <li><a href="patient.php">Patient Information</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Data</h1>
         <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Latitude</th>
                  <th>Longitude</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                </tr>
              </thead>
              <tbody>
                  <?php 
		  if($result!==FALSE){
		     while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
		        printf("<tr><td> &nbsp;%s </td><td> &nbsp;%s&nbsp; </td><td> &nbsp;%s&nbsp; </td><td> &nbsp;%s&nbsp; </td><td> &nbsp;%s&nbsp; </td></tr>", 
		           $row["date"], $row["latitude"], $row["longitude"], $row["Fname"], $row["Lname"]);
		     }
		     mysqli_free_result($result);
		     mysqli_close($link);
		  }
                    ?>
                  
                 
       
		 
       
              </tbody>
            </table>
          </div>
   
       
          
          
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
    </body>
</html>