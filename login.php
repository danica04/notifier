<!DOCTYPE html>
   <html>
    <head></head>

        <title>WELCOME</title>
      
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-responsive.css" />

    <link href="css/dashboard.css" rel="stylesheet">
    <link href="css/carousel.css" rel="stylesheet">
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        

        
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="login.php">E - Notifier Wristband: Chronic Patients Tracker System</a>
        </div>
        <div class="navbar-collapse collapse">
        </div>
      </div>
    </div>

        <div class="row" style="margin-top: 50px">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <fieldset>
                    <h3 class="Page-header">E - Notifier Wristband: Chronic Patients Tracker System</h3>
						
            <div class="form-group">
        <form action="verifylogin.php" method="POST" name="login">
		
    Username: <span class="glyphicon glyphicon-user"></span><input class="form-control" type="text" name="username" size="20" maxlength="20" required autofocus><br>
    Password: <span class="glyphicon glyphicon-lock"></span><input class="form-control" type="password" name="password" size="20" maxlength="20" required>
    <br />
    <span>Don't have an Account? <a href="register.php">Create an Account</a></span><br/>
    <span>Forgot your <a href="forgotPass.php">Password?</a></span><br/>
    <span>Login as <a href="admin.php">Admin</a></span> 
</div>

<input type="submit" value="Sign In" class="pass-reset-submit btn btn-success btn-sm">
        </form>
            </div>
                </fieldset>
            </div>
            <div class="col-lg-3"></div>
        </div>
        </div>

</body>
</html>